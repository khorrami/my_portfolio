$(function () {
    $("body").on('click', '.nav-toggle', function () {
        $("body").toggleClass('nav-open');
    }).on('click', '.nav__item', function () {
        let hasSubMenu = $(this).hasClass('nav__has__submenu');
        if (!hasSubMenu) {
            $("body").removeClass('nav-open');
        } else {
            $(this).toggleClass('nav__item_open');
        }
    }).on('click', '.nav__has__submenu > a', function (e) {
        e.preventDefault()
    })
})